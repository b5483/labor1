# Labor 1

## Bevezetés

Az Üzemeltetés tantárgy DevOps témájú laborjai során egy nagyon egyszerű ```CI/CD pipeline```-t fogunk elkészíteni. A laborok során regisztrálunk egy ```GitLab``` fiókot, ahol a feladatot ténylegesen el fogjuk végezni. Rendelkezésre bocsátunk egy nagyon egyszerű ```Spring boot``` alkalmazást, amely köré a pipeline-t fogjuk megépíteni. Azonban ennek a pipeline-nek valahol futnia is kell, emiatt a GitLab projekt létrehozása utána elindítunk egy ún. ```GitLab-Runner``` -t a laborgépen, amely végre fogja hajtani az utasításokat, amelyeket a pipeline egyes lépései "kiadnak". Mindezt ```Docker``` segítségével, ```konténerekkel``` tesszük.

A példa pipeline tartalmazni fogja az alkalmazás lefordítását, unit és integrációs tesztelését, konténerizálását (Docker image-et készítünk belőle), végül pedig a "kitelepítését" a saját laborgépre, mindezt teljesen automatizáltan és folyamatba szervezve.

Minden szükséges utasítást és leírást ebben a dokumentumban találsz.

>>>
Az ehhez hasonló formátumú szekciókban magyarázatokat találsz. A feladat végrehajtásához nem szükségesek, viszont olyan információkat kapsz, amik segítik a megértést.
>>>

## Első labor alkalom

### Docker ellenőrzése a laborgépen
Első lépésben nyiss egy ```PowerShell```-t, és add ki az alábbi utasítást benne:
```
docker run --name hello-world --rm -d -p 8080:80 docker/getting-started
```

Az alábbihoz nagyon hasonló kimenetet kell láss:

<img src="images/docker_getting_started.png">

Az utasítás hatására a Docker letöltötte a ```getting-started``` nevű image-et - amely tartalmaz egy webszervert, előre legyártott tartalommal - majd el is indította a laborgépen egy ```hello-world``` névre hallgató konténer formájában és elérhetővé tette a webszerver 80-as portját a laborgép 8080-as portján. Ellenőrizzük le!

Nyiss egy böngészőt, és írd be:
```
http://localhost:8080
```

Az alábbihoz egy nagyon hasonló oldalt kell láss:
<img src="images/docker_getting_started_html.png">

`Amennyiben így van, mehetsz is tovább, a Docker rendben működik a laborgépen. Ha valami okból kifolyólag nem jön be ez az oldal, vagy hibát kapsz a kiadott parancs hatására, jelezd a laborvezetőnek!`

Miután végeztél az ellenőrzéssel, térj vissza a PowerShell-hez, majd add ki benne az alábbi utasításokat:
```
docker stop hello-world
docker ps
```

Az első parancs segítségével leállítod a ```hello-world``` névvel rendelkező konténert, majd a második utasítás kiadásával ellenőrizni tudod, hogy már nem fut a konténer.
Kimenetként egy ehhez hasonlót kell láss: 
<img src="images/docker_getting_started_stop.png">

>>>
A Docker futtatása Windows-on és Mac-en egy kicsit "trükkös". A Docker egy olyan Linux kernel szolgáltatásra épül (cgroup), amely a többi operációs rendszeren nem létezik, és nem is emulálható. Emiatt valójában az említett két OS használata során a Docker telepítésével egy virtuális gépet is feltelepít a szoftver. Ez a VM egy Linux, amelyen a Docker szerver oldali komponense fut. A hoszt oldali OS-en (Win vagy Mac) pedig a docker utasítás csak egy kliens eszköz. Minden alkalommal, amikor egy docker utasítást adsz ki, akkor az valójában egy virtuális hálózaton keresztül a VM-ben futó Docker daemon-hoz jut el, amely végrehajtja az utasítást. Pont emiatt a Docker-t élesben (vagyis production-ben) csak és kizárólag Linuxon szabad használni, hiszen ott natívan fut az operációs rendszeren. A fenti példában a konténer portjának "kiengedése" a hoszt gépre, valójában több virtuális hálózaton át történik, és nem garantálható a stabilitása. Ugyanez a helyzet abban az esetben is, ha fájlokat, mappákat szeretnénk felcsatolni egy konténerbe. Gondoljunk csak arra, hogy a fájlrendszeri jogosultságok teljesen mások Windows-on és Linuxon. Nem szerencsés összekeverni őket, még akkor sem, ha látszólag valami működik.
>>>

### GitLab fiók és projekt regisztrálása
A GitLab egy git alapú verziókezelő szoftver, amely rengeteg DevOps-os szolgáltatással van kiegészítve. Itt fogjuk tárolni a forráskódot, amelyhez az automatizált folyamatokat készítjük el. 

Először is látogasd meg a [https://gitlab.com/users/sign_up](https://gitlab.com/users/sign_up)-ot és regisztrálj magadnak egy fiókot!

`Olyan adatokat adj meg, hogy a következő laboron is szükséges lesz ide belépned!`

Az adataid megadása után küldeni fog a GitLab egy megerősítő e-mail, ezt el kell fogadnod a `Confirm your Account gombbal`!

Ez után be tudsz már jelentkezni a Gitlab oldalán:
[https://gitlab.com/users/sign_in](https://gitlab.com/users/sign_in)

Sikeres bejelentkezés után a GitLab érdeklődik pár információ után. Igazából mindegy mit adsz meg, de azért én az alábbit javaslom:

<img src="images/gitlab_welcome.png">

Az űrlap kitöltése és elfogadása után létre kell hozzad az első Gitlab-os csoportod (Group) és benne a projekted, amiben dolgozni fogsz. Javaslom, hogy csoportnak valamilyen egyedi nevet találj ki, pl. a vezetékneved és a keresztneved első betúje: Vincs Eszter -> vincse

<img src="images/gitlab_create_project.png">

```Hozdd létre a projektet a Create project gomb megnyomásával!```

Kis idő után elkészül a projekted, és a GitLab szeretné tudni, hogy szeretnél-e másnak is hozzáférést adni egy dialógusban - ```GitLab is better with colleagues``` felirattal. Egyszerűen zárd be, ez csak a saját projekted lesz!

Az első képernyő amivel találkozni fogsz, egy tutorial oldal. Erre nem lesz szükségünk, egyszerűen a bal oldalon látható menü sávból válaszd ki a ```Repository``` gombot. Miután megnyomtad, látható, hogy a git repository, ahol a forráskódot és a pipeline leírása lesz található, jelenleg egy ```main branchet```, illetve egyetlen ```readme.md``` fájlt tartalmaz.

### GitLab-Runner regisztrálása

Ahhoz, hogy automatizáltan, CI/CD folyamatokat tudjon végrehajtani a GitLab, szüksége van egy szoftverre, ami ezt ténylegesen meg is teszi. A koncepciónak az a lényege, hogy lehet bárhol ez a szoftver, annak a gépnek az erőforrásait fogja a feladatok végrehajtására használni, amelyen futtatjuk. A GitLab alapértelmezetten is biztosít ilyen Runnereket, de ezek használata egyrészt korlátos, másrészt mi pont, hogy a saját laborgépünkön szeretnénk ezeket a folyamatokat futtatni. A szoftvert nem egyszerűen feltelepítjük a számítógépre, hanem Docker konténer formájában indítjuk el, majd beregisztráljuk a projektünk számára, mint használandó erőforrás.

Először is lépj a projekt beállítások CI/CD részébe! 

<img src="images/gitlab_project_settings.png">

A ```Runners``` listaelem jobb oldalán található egy ```Expand``` gomb, nyomd meg, amivel lenyílik!

<img src="images/gitlab_runners.png">

A képen két fontos dolog látható. Elsőnek az ```Enable shared runners for this project``` kapcsolót állítsuk át pipából X-be. Ezért azért tesszük, hogy ne a GitLab által biztosított Runnereket használjuk a feladat során, hanem majd a sajátunkat.

A bal oldalon a kisatírozott résznél látható az ún. ```Registration token```. Amikor elindítjuk majd a saját Runnerünket, be kell azt regisztráljuk a GitLab-hez, és ennek a tokennek a megadásával köthetjük össze a projektünkkel. ```Másold el magadnak valahová ezt a tokent! Lehet jegyzettömb, bármi, később kelleni fog!```

Térj vissza a laborgépen a PowerShell-be, és másold be az alábbi utasítást:

```
mkdir gitlab-runner
cd gitlab-runner
mkdir config
```

Ezzel létrehoztunk az aktuális mappában egy gitlab-runner nevű mappát, majd beleléptünk és készítettünk egy config nevű mappát is. Itt add ki az alábbi utasítást:

```
docker run -d --name gitlab-runner -v ${PWD}/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock quay.io/inotaykrisztian/gitlab-runner:latest
```

Kimenetként egy ehhez hasonlót kell láss: 

<img src="images/gitlab_runner_started.png">

A kiadott parancs lefutása után visszakapod a kurzort, és ha ismét kiadod a ```docker ps``` utasítást, akkor megnézheted, hogy éppen milyen konténerek futnak. Ahogy a képen is látható, meg kellett jelenjen a ```gitlab-runner``` nevezetű.

> Az utasítás egyes részei így értelmezhetőek:
>
> `docker run` - indíts el egy konténert
>
> `-d` - silent módban, vagyis háttérben futtasd (ellenkező esetben nem kapnánk vissza a kurzort)
>
> `--name gitlab-runner` - legyen gitlab-runner a konténer neve. Ellenkező esetben generálna valamit magától
>
> `-v ${PWD}/config:/etc/gitlab-runner` - az aktuális (PWD) mappának a config mappáját csatold be a konténerbe, és kerüljön a /etc/gitlab-runner mappába. Linuxon a Gitlab Runner a /etc/gitlab-runner mappában keresi a saját konfigurációját majd, és ide is hozza létre. Amikor majd beállítjuk a Runnert, az /etc/gitlab-runner/config mappába fogja létrehozni a config állományt. Viszont ezt szeretnénk megtartani hosszú ideig, ezért a saját gépünk egyik mappáját mountoljuk, vagyis csatoljuk bele. Ekkor ha bármilyen fájlművelet történik a linuxos mappában, valójában a saját mappánkban fog végrehajtásra kerülni. Így meg tudjuk őrizni a beállításainkat a konténer leállítása és újraindítása után is. Ha nem így tennénk, akkor a konténer újbóli indítása után a config fájlunk egyszerűen eltűnne (hiszen nem része az alap image-nek, amelyből fut)
>
> `-v /var/run/docker.sock:/var/run/docker.sock` - csatold be a hoszt gép /var/run/docker.sock nevű állományát a konténerbe ugyanezen az útvonalon. Ez több ponton is izgalmas. Először is, Windows alatt nincs is ilyen útvonal... Akkor mégis hogyan? Ahogy korábban említettem, Windows-on egy VM-ben fut a Docker daemon, ami Linux. Vagyis ez az útvonal nem a hoszt gépen, hanem abban a VM-ben létezik, amiben fut a Docker valójában. És hogy mit takar ez a docker.sock nevű fájl? Valójában ő a Docker daemon végpontja. Egy TCP/IP socket, amely egy tényleges hálózati kapcsolatot reprenzentál (igen, Linuxon fájlokban látható a proci, memória és a hálózat is). Ha becsatoljuk bármelyik konténerbe pont erre az útvonalra, akkor a docker utasítások kiadása a hoszt gépen (vagy a VM-ben) futó Docker daemon hálózati elérését adtuk valójában át.
>
> `quay.io/inotaykrisztian/gitlab-runner:latest` - használd ezt az image-et a konténer elkészítéséhez. A Docker image-ekből készíti el a konténereket. A konténer az az image, ami fut. És hogy miért nem volt jó az eredeti Docker Hub-on található hivatalos image? Azért, mert a Docker Hub használata IP címenként mennyiségi korlátos. Minden további nélkül elképzelhető lenne, hogy a labor végzése közben néhányatoknak, vagy mindannyiótoknak azt az üzenetet kellene látnotok, hogy az adott időszakban az egyetem már elhasználta a letöltési kvótát és nincs tovább. Ezért az image-eket egy olyan helyre tettem fel, ahol nincs ilyen megkötés.


Már csak annyi dolgunk van, hogy be is regisztráljuk a GitLab fiókunkba. Ehhez annyit kell tennünk, hogy belépünk a már futó konténerbe, és kiadjuk a regisztrációs parancsot. Ehhez add ki az alábbi utasítást a PowerShell-ben: (annyit tesz, hogy beléptet a gitlab-runner nevű konténerbe, és a bash nevű linuxos parancsértelmezőt fogja elindítani számunkra, ahol kiadhatjuk az utasítást)

```
docker exec -it gitlab-runner bash
```

Egy ehhez hasonló sztringnek kell megjelennie a konzolon: `root@4ae5e4f6863a:/#˙`

Add ki az alábbi utasítást: (immár a futó konténer belsejében)

```
gitlab-runner register
```

Ennek hatására elindul egy szöveges "varázsló", ahol meg kell adjunk néhány adatot.
<img src="images/gitlab_runner_register.png">

```
Instance URL: https://gitlab.com

Registration token: a GitLab felületéről elmásolt tokent kell ide beilleszteni. Tedd csak vágólapra, majd jobb klikk a PowerShell-ben! Ennyi a beillesztés.

Description: runner

Tags: a tag-ek segítségével jelezhetjük a CI/CD folyamatban, hogy mely runner futtassa az adott folyamatot. Adjuk meg, hogy docker

Optional maintenance note: csak üss egy entert és várj, nem adunk meg ilyet

Executor: docker

Default Docker Image: quay.io/inotaykrisztian/bash
```

Siker esetén egy ehhez hasonló kimenetet kell lássunk:
<img src="images/gitlab_runner_registered.png">

Írd be, hogy `exit`, majd üss entert! Ekkor visszatérsz a konténer belsejéből a saját terminálodba.

Egy `cd config` utasítás segítségével lépj a config mappába és `ls`-sel listázd ki a tartalmát! Láthatod, hogy létrejött benne egy `config.toml` nevű állomány. Ez az immáron beregisztrált Gitlab-Runner konfigurációs állománya, minden olyan adattal, amit megadtunk a szöveges "varázsló"-nak korábban. Ez úgy lehetséges, hogy a `docker run....` utasításban található egy ilyen rész: `-v ${PWD}/config:/etc/gitlab-runner`. Ezzel arra kértük a docker-t, hogy annak a mappának, ahol épp kiadjuk a parancsot, a config almappáját mountolja be (csatolja be) a konténerben lévő /etc/gitlab-runner mappába. Így amit a konténer ezzel a mappával tesz, az a saját gépünkön is látható marad. Ez az egyik módja annak, hogy állományokat tudjunk megőrizni a konténerek indítása és leállítása között. Egyébként minden amit beállítottunk elveszne. 

A következő labor során egy Docker image-et fogunk legyártatni ezzel a szoftverrel, azonban ezzel a beállítással erre még nem képes. Egy jegyzet alkalmazással (Jegyzettömb, NotePad++, **NEM** Word) nyisd meg a config.toml-t!

Egy ehhez hasonló struktúrát kell látnod:
```yaml
concurrent = 3
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "runner"
  url = "https://gitlab.com"
  token = "********************"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "bash"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
    shm_size = 0
```

A **runners.docker** szekcióban találod a `privileged = false ` sort. Írd át `privileged = true`-ra! Ezzel jogosultságot adunk a Runner számára, hogy `root` (admin) felhasználóként is futtathasson konténereket.

Továbbá a **volumes** részt is ki kell egészíteni. Nyugodtan másold át az egész volumes-os sort a saját állományodba! Erre azért van szükség, mert így gyakorlatilag átadjuk minden GitLab Runner által elindított konténernek a hoszt gépen futó Docker daemont-t. Ez által a GitLab CI/CD-ben kiadott docker parancsok pontosan ugyanazt fogják csinálni, mintha egy ember kézzel, a PowerShell ablakban adta volna ki őket.

Végezetül a `concurrent` nevű első sorban található értéket módosítsd 3-ra! Ezzel azt határozzuk meg, hogy párhuzamosan hány feladatot, vagy jobot tud futtatni a runner.

Mentsd el a fájlt!

`Mindegy milyen eszközzel, de ezt a fájlt küldd most el saját magadnak e-mailben!` Azért van erre szükség, mert a második laboron is használjuk majd a runnert, így ha megőrizzük ezt a fájlt, akkor következő alkalommal nem kell majd újra beállítani azt!

Következő lépésben újra kell indítsuk a konténert, hogy teljesen biztosan érvényre lépjenek a beállítások, amiket megadtunk. Ehhez add ki az alábbi utasítást:

```
docker restart gitlab-runner
docker logs gitlab-runner
```

Az első sor újraindítja a konténert, míg a második kiírja a naplóját. Hasonló kimenetet kell láss:
<img src="images/gitlab_runner_logs.png">

Ez után menj vissza a GitLab weboldalára, amit ott hagytunk a Runner beállításoknál (Project nézet, Settings->CI/CD->Runners). Üss egy újratöltést, vagy nyomj F5-öt! A Runners szekciónál válaszd ismét az `Expand` gombot! Látnod kell a frissen beregisztrált saját Gitlab-Runner példányod!

<img src="images/gitlab_registered_runner.png">

A labor utolsó feladataként hajtsunk végre vele egy CI/CD feladatot, amolyan Hello World-öt!

### Hello World! pipeline létrehozása és futtatása

A GitLab felületén a bal oldali menüsávban klikkeljünk a `Repository` menüpontra. Ekkor megjelenik a repository jelenlegi tartalma, a readme fájl, illetve annak renderelt változata.

Először is hozzunk létre egy `develop` nevű branchet! Ezt könnyen megtehetjük itt a felületen, mindössze a `main` branch mellett megjelenő `+` jelre kell klikkel, és kiválasztani a `new branch` feliratú lehetőséget.

<img src="images/gitlab_new_branch.png">

Az új branch neve legyen `develop`, majd `Create branch`!

Mivel innentől kezdve ezen a branchen fogunk dolgozni, állítsuk be, hogy mindig ezt lássuk, ahányszor megnyomjuk a Repository gombot!

Ehhez a bal oldali sávban válasszuk ki a Settings->Repository menüt.

<img src="images/gitlab_settings_repository.png">

A megjelenő lista első eleme a `Default branch`, ez határozza meg, hogy az egész repository-nak mi az alapértelmezett branch-e. Nyissuk le `Expand`-dal, majd válasszuk ki a `develop`-ot! `Save changes`-szel mentsük el, majd a bal oldali sávos menüből válasszuk ki ismét a `Repository`-t!

A középső terület tetején látható, hogy immáron a `develop` az aktuális branch-ünk kiválasztva. Valamint ez így is marad a későbbiekben, ahányszor csak idejövünk.

<img src="images/gitlab_default_develop.png">

Utolsó lépésként a `develop` melletti `+` megnyomása után hozzunk létre egy új fájlt!

<img src="images/gitlab_new_file.png">

Fájl névnek másold be az alábbit: `.gitlab-ci.yml`
A GitLab pontosan az ilyen nevű fájlban fogja keresni minden projektben (alapértelmezés szerint legalábbis) a CI/CD folyamatokat leíró fájlt. 

A fájl tartalmának másold be az alábbit:

```yaml
stages:
  - hello

hello-world:
  stage: hello
  tags:
    - docker
  script:
    - echo "Hello World!"
  image: "quay.io/inotaykrisztian/bash"

```

A leírás az alábbit tartalmazza, ha móricka-nyelven szeretném megfogalmazni:
Szeretnék egy pipeline "stage"-et, aminek egyetlen lépése van, a neve legyen hello-world. Ez a lépés csak olyan GitLab-Runneren futhat, amelynek a tag-jei között megtalálható a docker. Amikor elindul a lépés, akkor a Runner töltsön le egy bash nevű docker image-et, indítsa el, és a kiadott utasításokat a konténer belsejében hajtsa végre. Egy utasítást adunk ki, ez annyit tesz, hogy kiírja a konzolra, hogy Hello World!. A lépés minden esetben végrehajtásra kerül, amikor bármi történik a git repository-val (pl. módosítunk, vagy fájlokat töltünk fel bele.). Ennnél részletesebb magyarázatot a második laboron adunk!

Nyomjunk rá a `Commit` gombra! Ekkor az új fájlunk felkerül a repository `develop` branchére, aminek hatására el is indul a CI/CD folyamat. Ez a bal oldali menü CI/CD pontjára kattintva nézhetjük meg.

<img src="images/gitlab_cicd_first_run.png">

Minden bizonnyal Nálatok még a zöld passed gomb egy kék `running` lesz. Ez jelzi, hogy fut a folyamat. Nyomj rá erre a gombra, mindegy milyen a színe! Ekkor a pipline nézetbe kerülsz, ahol láthatod, hogy az adott futás során milyen stage-ekben milyen lépéseket hajt végre a GitLab.

<img src="images/gitlab_cicd_first_pipeline.png">

A pirossal aláhúzott `Hello` a stage neve. A stage-eket a GitLab-ban csoportosításra használhatjuk. A stage-eknek `job`-jai, vagyis feladatai vannak. Azonos stage-ben lévő feladatok párhuzamosan futnak egymással, míg az egyes stage-ek akkor jönnek egymás után, ha a bennük lévő feladatok közül mind lefutott. Ezzel a két építőelemmel tudunk különöböző folyamatokat felépíteni GitLab-ban.

Jelenleg a `Hello` stage-nek egyetlen `job`-ja van, ez pedig a `hello-world`. Klikkelj rá!

Ha minden rendben ment, akkor kis idő elteltével ehhez hasonló kimenetet kell láss:

<img src="images/gitlab_cicd_helloworld.png">

Pirossal aláhúztam, hogy egyrészt látható a kiadott utasítás (zölddel), majd alatta az utasítás eredménye (szürkével). Mindez pedig a saját laborgépünkön futott le egy konténerben. Látható szépen visszafelé a napló soraiban, ahogy először a GitLab kiválasztja a runner-t, majd a runner letölti a bash docker image-et, amiben dolgozni fog. Ez után a git repository-nk tartalmát is letölti a konténer belsejében (hiszen egy CI/CD folyamat a repository tartalmával dolgozik általában). Utolsó lépésként amikor minden inicializált, kiadja az utasítást, amit a pipeline leíró scriptben adtunk neki.

Ezzel az első labor véget ért.

`Ne feledd, hogy tedd el magadnak a gitlab-runner config mappájában található config.toml-t! Valamint őrizd meg ezt a repository-t, mert innét megyünk tovább, hogy építsünk egy igazi alkalmazáshoz egy igazi CI/CD folyamatot!`

